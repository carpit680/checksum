#include <string>
#include <cstring>
// using namespace std;

namespace checksum {
    class Checksum {
            private:
            std::string received_data_;
            uint8_t received_checksum_;
            uint8_t data_size_;

        public:
            Checksum() = default;
            ~Checksum() = default;
            Checksum(std::string data);
            uint8_t get_checksum(std::string data);
            bool check_checksum();
    };
}