//[(1byte for data size)(data bytes)(1 byte checksum)]

#include "checksum.h"
#include <iostream>

using namespace checksum;

Checksum::Checksum(std::string data) { 

    memcpy(&data_size_, &data, 1);
    // data_type_ = (type_and_size_ & 11000000)>>6;
    // data_size_ = type_and_size_ & 00111111;
    std::memcpy(&received_data_, &data+1, data_size_ );
    std::memcpy(&received_checksum_, &data + data_size_, 1);
}

bool Checksum::check_checksum() {
    uint8_t sum = received_data_[0];
    for(int i=1; i < (data_size_+1);i++) {
        sum ^= received_data_[i];
    }

    if((sum << 1) == sum) 
        return true;
    else 
        return false;
}

uint8_t Checksum::get_checksum(std::string data) {
    uint8_t checksum = data[0];
    for(int i=1; i < data_size_;i++) {
        checksum ^= data[i];
    }
    checksum ^= 255;
     return checksum;
}
